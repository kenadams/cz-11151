﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;

namespace NLProcessing
{
    public class NLProcessor
    {
        private const int _PRE_INIT = 0;
        private const int _INITIALIZING = 1;
        private const int _READY = 2;

        private int _STATE = _PRE_INIT;

        private static Task _initialization;
        private static NLProcessor _instance = new NLProcessor();
        private Dictionary<String, ISet<String>> _synonyms;
        private Dictionary<String, ISet<String>> _diminutives;
        private Dictionary<String, IEnumerable<String>> _kurnik;
        private Dictionary<String, String> _basics;
        private Dictionary<String, String> idMap;

        private void Initialize()
        {
            lock (_instance)
            {
                //Console.WriteLine("Initializing...");
                XPathDocument _wordnet = new XPathDocument("../../../NLProcessor/plwordnet_2_0.xml");
                _synonyms = new Dictionary<String, ISet<String>>();
                _diminutives = new Dictionary<String, ISet<String>>();
                idMap = new Dictionary<String, String>();

                XPathNavigator wordNavigator = _wordnet.CreateNavigator();
                XPathNodeIterator lexicalUnitIterator = wordNavigator.Select("/array-list/lexical-unit");
                XPathNodeIterator synsetIterator = wordNavigator.Select("/array-list/synset");
                XPathNodeIterator lexicalRelationsIterator = wordNavigator.Select("/array-list/lexicalrelations");

                ISet<HashSet<String>> synsets = new HashSet<HashSet<String>>();

                //Console.WriteLine("Done loading.");
                foreach (XPathNavigator oCurrentWord in lexicalUnitIterator)
                {
                    string id = oCurrentWord.GetAttribute("id", "").Trim();
                    string name = oCurrentWord.GetAttribute("name", "");
                    idMap.Add(id, name);
                }

                foreach (XPathNavigator oCurrentSynset in synsetIterator)
                {
                    XPathNodeIterator currentSynsetIterator = oCurrentSynset.Select("unit-id");
                    HashSet<String> currentSynset = new HashSet<String>();

                    foreach (XPathNavigator currentUnitId in currentSynsetIterator)
                    {
                        string currentId = currentUnitId.Value.Trim();
                        currentSynset.Add(currentId);
                    }

                    synsets.Add(currentSynset);
                }

                //Console.WriteLine("Done creating id map and synsets.");

                foreach (HashSet<String> currentSynset in synsets)
                {
                    foreach (String currentId in currentSynset)
                    {
                        HashSet<String> nameSet = new HashSet<string>();
                        foreach (String id in currentSynset)
                        {
                            if (id != currentId)
                                nameSet.Add(idMap[id]);

                        }

                        string currentName = idMap[currentId];

                        if (!_synonyms.ContainsKey(currentName))
                            _synonyms.Add(currentName, nameSet);
                        else
                            _synonyms[currentName].UnionWith(nameSet);
                    }
                }

                //Console.WriteLine("Done creating synonyms.");


                foreach (XPathNavigator oCurrentLexicalRelation in lexicalRelationsIterator)
                {
                    if (oCurrentLexicalRelation.GetAttribute("relation", "") == "56")
                    {
                        string child = idMap[oCurrentLexicalRelation.GetAttribute("child", "")];
                        string parent = idMap[oCurrentLexicalRelation.GetAttribute("parent", "")];
                        HashSet<String> parents = new HashSet<String>();
                        parents.Add(parent);
                        if (!_diminutives.ContainsKey(child))
                            _diminutives.Add(child, parents);
                        else
                            _diminutives[child].UnionWith(parents);
                    }
                }

                //Console.WriteLine("Done creating diminutives.");

                _kurnik = new Dictionary<string, IEnumerable<string>>();
                _basics = new Dictionary<string, string>();
                foreach (String line in File.ReadAllLines("C:/Users/Grzegorz.ASUS/Documents/Frazeusz/odm.txt"))
                {
                    string[] items = line.Split(',');
                    IList<string> list = new List<string>();

                    items[0] = items[0].Trim();

                    for (int i = 1; i < items.GetLength(0); ++i)
                    {
                        items[i] = items[i].Trim();
                        list.Add(items[i]);
                        if (!_basics.ContainsKey(items[i]))
                        {
                            _basics.Add(items[i], items[0]);
                        }

                    }
                    if (_kurnik.ContainsKey(items[0]))
                        _kurnik[items[0]].ToList().AddRange(list);
                    else
                        _kurnik.Add(items[0], list);

                }
                //Console.WriteLine("Done creating forms.");
                _STATE = _READY;
            }
        }


        public bool IsReady()
        {
            return _STATE == _READY;
        }

        public static void Init()
        {
            lock (_instance)
            {
                if (_instance._STATE == _PRE_INIT)
                {
                    _instance._STATE = _INITIALIZING;
                    Action initAction = _instance.Initialize;
                    _initialization = new Task(initAction);
                    _initialization.Start();
                }
            }
        }

        private NLProcessor() { }

        public static NLProcessor Instance()
        {
            NLProcessor.Init();
            _initialization.Wait();
            return _instance;
        }

        public Dictionary<String, IEnumerable<String>> Forms(IEnumerable<String> words)
        {
            Dictionary<String, IEnumerable<String>> formsDictionary = new Dictionary<String, IEnumerable<String>>();

            foreach (string word in words)
            {
                try
                {
                    formsDictionary.Add(word, _kurnik[word]);
                }
                catch (KeyNotFoundException e)
                {
                    throw new NonExistingWordException("There are no forms for word " + word, e);
                }
            }

            return formsDictionary;
        }


        public Dictionary<String, IEnumerable<String>> Synonyms(IEnumerable<String> words)
        {
            Dictionary<String, IEnumerable<String>> synonyms = new Dictionary<String, IEnumerable<String>>();

            foreach (String word in words)
            {
                try
                {
                    synonyms.Add(word, _synonyms[word]);
                }
                catch (KeyNotFoundException e)
                {
                    throw new NonExistingWordException("There are no synonyms for word " + word, e);
                }
            }
            return synonyms;
        }

        public Dictionary<String, IEnumerable<String>> Diminutives(IEnumerable<String> words)
        {

            Dictionary<String, IEnumerable<String>> diminutives = new Dictionary<String, IEnumerable<String>>();

            foreach (String word in words)
            {
                try
                {
                    diminutives.Add(word, _diminutives[word]);
                }
                catch (KeyNotFoundException e)
                {
                    throw new NonExistingWordException("There are no diminutives for word " + word, e);
                }
            }
            return diminutives;
        }

        private String BasicForm(String word)
        {
            string basic;
            try
            {
                basic = _basics[word];
            }
            catch (KeyNotFoundException e)
            {
                throw new NonExistingWordException("There is no basic form for word " + word, e);
            }
            return basic;
        }

    }
}
