﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLProcessing
{
    public class NonExistingWordException : KeyNotFoundException
    {

        public NonExistingWordException() : base() { }
        public NonExistingWordException(string message) : base(message) { }
        public NonExistingWordException(string message, KeyNotFoundException inner) : base(message, inner) { }
    }
}
