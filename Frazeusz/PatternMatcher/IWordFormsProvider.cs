﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frazeusz.PatternMatcher
{
    using WordDict = Dictionary<String, IEnumerable<String>>;

    public interface IWordFormsProvider
    {

        WordDict getForms(IEnumerable<String> words);
        WordDict getInflections(IEnumerable<String> words);
        WordDict getSynonyms(IEnumerable<String> words);
        WordDict getDiminutives(IEnumerable<String> words);
        WordDict getPlural(IEnumerable<String> words);
    }
}
