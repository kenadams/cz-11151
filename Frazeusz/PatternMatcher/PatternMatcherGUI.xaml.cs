﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.TeamFoundation.MVVM;
using PatternMatcher;

namespace Frazeusz.PatternMatcher
{
    /// <summary>
    /// Interaction logic for PatternMatcherGUI.xaml
    /// </summary>
    public partial class PatternMatcherGUI : Page, IPatternGUI
    {
        private int _idSequence = 0;
        private readonly IWordFormsProvider _formsProvider;

        public List<PatternMatcher> Patterns { get; private set; }
        public BindingList<PatternMatcher> ListModel { get; private set; }
        public PatternMatcher CurrentSelectedMatcher { get; set; }

        public ICommand AddCommand { get; set; }
        public ICommand RemoveCommand { get; set; }

        public string PatternText { get; set; }

        public PatternMatcherGUI()
            : this(new DummyWordsProvider())
        {
            InitializeComponent();
        }

        public PatternMatcherGUI(IWordFormsProvider provider)
        {
            _formsProvider = provider;
            Patterns = new List<PatternMatcher>();
            ListModel = new BindingList<PatternMatcher>(Patterns);
            InitCommands();
        }

        private void InitCommands()
        {
            AddCommand = new RelayCommand(AddPattern);
            RemoveCommand = new RelayCommand(RemovePattern);
        }

        public void AddPattern()
        {
            AddPattern(PatternText, PatternType.Forms);
        }

        private void AddPattern(string text, PatternType type)
        {
            if (PatternExists(text))
            {
                ShowError("Pattern already added");
            }
            else
            {
                try
                {
                    var p = new Pattern(GenId(), text, type);
                    var matcher = new PatternMatcher(p, _formsProvider);
                    ListModel.Add(matcher);
                }
                catch (Exception e)
                {
                    ShowError(e.Message);
                }
            }
        }

        private static void ShowError(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButton.OK, 
                MessageBoxImage.Error);
        }

        private bool PatternExists(string pattern)
        {
            return Patterns.Exists((x) => x.SameAs(pattern));
        }

        public void RemovePattern()
        {
            ListModel.Remove(CurrentSelectedMatcher);
        }

        private int GenId()
        {
            return Interlocked.Increment(ref _idSequence);
        }

    }
}
