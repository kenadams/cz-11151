﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frazeusz.PatternMatcher
{
    public class Pattern
    {
        public int patternId { get; private set; }
        public string pattern { get; private set; }
        public PatternType patternType { get; private set; }

        public Pattern(int id, string pattern, PatternType type)
        {
            this.patternId = patternId;
            this.pattern = pattern;
            this.patternType = type;
        }
    }
}
