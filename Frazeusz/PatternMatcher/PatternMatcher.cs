﻿using System;
using System.Collections.Generic;
using System.Linq;
using Frazeusz.PatternMatcher;

namespace Frazeusz.PatternMatcher
{
    public class PatternMatcher : IPatternMatcher
    {
        private readonly Pattern _pattern;
        private readonly IEnumerable<string> _words; 
        private readonly IWordFormsProvider _formsProvider;
        private readonly Dictionary<String, IEnumerable<String>> _dictionary;

        public PatternMatcher(Pattern pattern, IWordFormsProvider formsProvider)
        {
            ValidatePattern(pattern.pattern);

            this._pattern = pattern;
            this._formsProvider = formsProvider;

            _words = SplitPattern(pattern.pattern);

            switch (pattern.patternType)
            {
                case PatternType.Forms:
                    _dictionary = formsProvider.getForms(_words);
                    break;
                case PatternType.Inflections:
                    _dictionary = formsProvider.getInflections(_words);
                    break;
                case PatternType.Plural:
                    _dictionary = formsProvider.getPlural(_words);
                    break;
                case PatternType.Synonyms:
                    _dictionary = formsProvider.getSynonyms(_words);
                    break;
            }
        }

        private static void ValidatePattern(string pattern)
        {
            if (String.IsNullOrEmpty(pattern))
            {
                throw new ArgumentException("Pattern must be nonempty");
            }
        }

        private static IEnumerable<string> SplitPattern(string pattern)
        {
            char[] c = {' '};
            return pattern.Split(c, StringSplitOptions.RemoveEmptyEntries);
        }

        public new string ToString
        {
            get { return string.Join(" | ", _words); }
        }

        public bool SameAs(string pattern)
        {
            return _words.SequenceEqual(SplitPattern(pattern));
        }

        public int getId()
        {
            return _pattern.patternId;
        }

        public string FindPattern(string sentence)
        {
            return _words.Any(sentence.Contains) ? sentence : null;
        }
    }
}
