﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frazeusz.PatternMatcher
{
    interface IPatternGUI
    {
        List<global::Frazeusz.PatternMatcher.PatternMatcher> Patterns { get; }
    }
}
