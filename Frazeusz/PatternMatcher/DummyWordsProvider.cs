﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Frazeusz.PatternMatcher;

namespace PatternMatcher
{
    class DummyWordsProvider: IWordFormsProvider
    {
        public Dictionary<string, IEnumerable<string>> getForms(IEnumerable<string> words)
        {
            return new Dictionary<string, IEnumerable<string>>();
        }

        public Dictionary<string, IEnumerable<string>> getInflections(IEnumerable<string> words)
        {
            return new Dictionary<string, IEnumerable<string>>();
        }

        public Dictionary<string, IEnumerable<string>> getSynonyms(IEnumerable<string> words)
        {
            return new Dictionary<string, IEnumerable<string>>();
        }

        public Dictionary<string, IEnumerable<string>> getDiminutives(IEnumerable<string> words)
        {
            return new Dictionary<string, IEnumerable<string>>();
        }

        public Dictionary<string, IEnumerable<string>> getPlural(IEnumerable<string> words)
        {
            return new Dictionary<string, IEnumerable<string>>();
        }
    }
}
