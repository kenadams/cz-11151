﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLProcessing;
using System.Collections.Generic;

namespace NLProcessorTests
{
    [TestClass]
    public class NLProcessorTests
    {
        [TestMethod]
        public void Forms_WithExistingWord()
        {
            NLProcessor nlp = NLProcessor.Instance();
            string wordForTesting = "dom";
            IList<String> words = new List<string>();
            words.Add(wordForTesting);

            Dictionary<String, IEnumerable<String>> result = nlp.Forms(words);

            Assert.IsTrue(result.ContainsKey(wordForTesting));
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistingWordException))]
        public void Forms_WithNonExistingWord()
        {
            NLProcessor nlp = NLProcessor.Instance();
            string wordForTesting = "siabka";
            IList<String> words = new List<string>();
            words.Add(wordForTesting);

            Dictionary<String, IEnumerable<String>> result = nlp.Forms(words);

        }

        [TestMethod]
        public void Synonyms_WithExistingWord()
        {
            NLProcessor nlp = NLProcessor.Instance();
            string wordForTesting = "dom";
            IList<String> words = new List<string>();
            words.Add(wordForTesting);

            Dictionary<String, IEnumerable<String>> result = nlp.Forms(words);

            Assert.IsTrue(result.ContainsKey(wordForTesting));
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistingWordException))]
        public void Synonyms_WithNonExistingWord()
        {
            NLProcessor nlp = NLProcessor.Instance();
            string wordForTesting = "siabka";
            IList<String> words = new List<string>();
            words.Add(wordForTesting);

            Dictionary<String, IEnumerable<String>> result = nlp.Forms(words);

        }

        [TestMethod]
        public void Diminutives_WithExistingWord()
        {
            NLProcessor nlp = NLProcessor.Instance();
            string wordForTesting = "dom";
            IList<String> words = new List<string>();
            words.Add(wordForTesting);

            Dictionary<String, IEnumerable<String>> result = nlp.Forms(words);

            Assert.IsTrue(result.ContainsKey(wordForTesting));
        }

        [TestMethod]
        [ExpectedException(typeof(NonExistingWordException))]
        public void Diminutives_WithNonExistingWord()
        {
            NLProcessor nlp = NLProcessor.Instance();
            string wordForTesting = "siabka";
            IList<String> words = new List<string>();
            words.Add(wordForTesting);

            Dictionary<String, IEnumerable<String>> result = nlp.Forms(words);

        }
    }
}
