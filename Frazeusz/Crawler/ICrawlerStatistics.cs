﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crawler
{
    public interface ICrawlerStatistics
    {
        ConcurrentDictionary<Uri /*base uri*/, int> TotalCrawled { get; }
    }
}
