﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crawler
{
    class Program
    {
        static void Main(string[] args)
        {
            Crawler c = new Crawler(new Configuration());

            ICrawlerResultsQueue rq = c.ResultsQueue;
            ICrawlerUrisQueue uq = c.UrisQueue;

            c.EnqueueBaseDomain(new Uri("http://www.agh.edu.pl"), 1, 1);
            c.EnqueueBaseDomain(new Uri("http://google.com"), 1, 1);

            c.Start();

            while(true)
            {
                bool b;
                var t = rq.Take(out b);
                Console.WriteLine(t.Content);
            }
        }
    }
}
