﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crawler
{
    public struct CrawlerResult
    {   
        public CrawlerResult(String content, Uri uri, SearchableContentTypes contentType)
        {
            this.Content = content;
            this.Uri = uri;
            this.ContentType = contentType;
        }

        public String Content;
        public Uri Uri;
        public SearchableContentTypes ContentType;
    }

    public interface ICrawlerResultsQueue
    {
        // bool indicated whether to enqueue found uris in the text or not
         CrawlerResult Take(out bool shouldEnqueueUrls);
    }
}
