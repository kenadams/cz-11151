﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.IO;

namespace Crawler
{

    public enum SearchableContentTypes {
        HTML,
        PDF
    }

    public class Configuration
    {
        // fake one
        public Configuration() { }

        public Configuration(IEnumerable<SearchableContentTypes> contentTypes)
        {
            validContentTypes = new List<SearchableContentTypes>(contentTypes);
        }

        private List<SearchableContentTypes> validContentTypes;

        IEnumerable<SearchableContentTypes> ValidContentTypes
        {
            get { return validContentTypes; }
        }
    }

    interface ICrawler
    {
        ICrawlerUrisQueue UrisQueue { get; }

        ICrawlerResultsQueue ResultsQueue { get; }

        ICrawlerStatistics Statistics { get; }

        bool IsRunning { get; }

        void Start();

        void Stop();

        void EnqueueBaseDomain(
            Uri baseDomain,
            int maxAcceptedInDomainGeneration,
            int maxAcceptedOuterDomainGeneration
       );
    }

    public class Crawler : ICrawler
    {
        private class CrawlerUrisQueue : ICrawlerUrisQueue
        {
            // TODO: may remove it, have childrenToProcess dict anyways
            private HashSet<Uri> processedUris;

            // tuple hold count of childeren to process in the base domain and in outer domains
            private Dictionary<Uri, Tuple<int, int>> childrenToProcess;

            // bool whether ask for child links
            // private ConcurrentQueue<Tuple<Uri, bool>> urisQueue;
            private BlockingCollection<Tuple<Uri, bool>> blockingQueueWrapper;

            public CrawlerUrisQueue()
            {
                processedUris = new HashSet<Uri>();
                //urisQueue = new ConcurrentQueue<Tuple<Uri, bool>>();
                blockingQueueWrapper = new BlockingCollection<Tuple<Uri, bool>>();
                childrenToProcess = new Dictionary<Uri, Tuple<int, int>>();
            }
            
            public void Enqueue(Uri parentUri, IEnumerable<Uri> uris)
            {
                IEnumerable<Uri> unprocessedUris;

                Tuple<int, int> inDomainChildCrawldDepth;
                Tuple<int, int> outerDomainChildCrawldDepth;

                lock (processedUris)
                {
                    // need to happen in lock, uses non thread-safe dictionary
                    Tuple<int, int> parentCrawlDepths = childrenToProcess[parentUri];

                    inDomainChildCrawldDepth = new Tuple<int, int>(parentCrawlDepths.Item1 - 1, parentCrawlDepths.Item2);
                    outerDomainChildCrawldDepth = new Tuple<int, int>(parentCrawlDepths.Item2 - 1, 0);

                    unprocessedUris = uris.Except(processedUris);

                    foreach (var uri in unprocessedUris) 
                    {
                        // TODO: check if current domain
                        if (true /* in domain */)
                        {
                            // if is zero should be still processed, just don't do any child processing
                            if (inDomainChildCrawldDepth.Item1 >= 0)
                            { 
                                processedUris.Add(uri);
                                childrenToProcess.Add(uri, inDomainChildCrawldDepth);
                                blockingQueueWrapper.Add(new Tuple<Uri, bool>(uri, inDomainChildCrawldDepth.Item1 > 0 || inDomainChildCrawldDepth.Item2 > 0));
                            } 
                        }
                        else  /* outer domain */
                        {
                            // if is zero should be still processed, just don't do any child processing
                            if (outerDomainChildCrawldDepth.Item1 >= 0)
                            {
                                processedUris.Add(uri);
                                childrenToProcess.Add(uri, outerDomainChildCrawldDepth);
                                blockingQueueWrapper.Add(new Tuple<Uri, bool>(uri, outerDomainChildCrawldDepth.Item1 > 0 || outerDomainChildCrawldDepth.Item2 > 0));
                            } 
                        }
                    }
                }
            }

            public void EnqueueBaseDomain(Uri baseDomain, int maxAcceptedInDomainGeneration, int maxAcceptedOuterDomainGeneration)
            {
                Tuple<int, int> parentCrawlDepths = new Tuple<int, int>(maxAcceptedInDomainGeneration, maxAcceptedOuterDomainGeneration);
                lock (processedUris)
                {
                    processedUris.Add(baseDomain);
                    childrenToProcess.Add(baseDomain, parentCrawlDepths);
                    blockingQueueWrapper.Add(new Tuple<Uri, bool>(baseDomain, parentCrawlDepths.Item1 > 0 || parentCrawlDepths.Item2 > 0));
                }
            }

            public bool Take(out Uri uri)
            {
                Tuple<Uri, bool> item = blockingQueueWrapper.Take();
                uri = item.Item1;
                return item.Item2;
            }
        }

        private class CrawlerResultsQueue : ICrawlerResultsQueue
        {
            private BlockingCollection<Tuple<CrawlerResult, bool>> blockingQueueWrapper;
            //private ConcurrentQueue<Tuple<CrawlerResult, bool>> resultsQueue;

            public CrawlerResultsQueue()
            {
                //resultsQueue = new ConcurrentQueue<Tuple<CrawlerResult, bool>>();
                blockingQueueWrapper = new BlockingCollection<Tuple<CrawlerResult, bool>>();
            }
            
            public void Enqueue(CrawlerResult result, bool askForUris)
            {
                blockingQueueWrapper.Add(new Tuple<CrawlerResult, bool>(result, askForUris));
            }

            public CrawlerResult Take(out bool shouldEnqueueUrls)
            {
                Tuple<CrawlerResult, bool> job = blockingQueueWrapper.Take();
                shouldEnqueueUrls = job.Item2;
                return job.Item1;
            }
        }

        private class CrawlerStatistics : ICrawlerStatistics
        {
            public CrawlerStatistics()
            {
                totalCrawled = new ConcurrentDictionary<Uri, int>();
            }

            public ConcurrentDictionary<Uri /*base uri*/, int> TotalCrawled { get { return totalCrawled; } }

            private ConcurrentDictionary<Uri /*base uri*/, int> totalCrawled;
        }

        class ResourceNotAvailableException : Exception
        {}

        class UnsupportedContentTypeException : Exception
        {}

        private Configuration configuration;

        private HttpClient httpClient;

        private CrawlerUrisQueue urisQueue;

        private CrawlerResultsQueue resultsQueue;

        private CrawlerStatistics statistics;

        private bool isRunning;

        private Thread workerThread;

        public ICrawlerUrisQueue UrisQueue
        {
            get { return urisQueue; }
        }

        public ICrawlerResultsQueue ResultsQueue
        {
            get { return resultsQueue; }
        }

        public ICrawlerStatistics Statistics
        {
            get { return statistics; }
        }

        public bool IsRunning
        {
          get { return isRunning; }
        }

        public void Start()
        {
            isRunning = true;
        }

        public void Stop()
        {
            isRunning = false;
        }

        public Crawler(Configuration configuration)
        {
            this.configuration = configuration;
            httpClient = new HttpClient();
            
            // TODO: setup accepted content types
            urisQueue = new CrawlerUrisQueue();
            resultsQueue = new CrawlerResultsQueue();
            statistics = new CrawlerStatistics();

            isRunning = true;

            workerThread = new Thread(new ThreadStart(RunDownloadingThread));
            workerThread.Start();
        }

        public void EnqueueBaseDomain(Uri baseDomain, int maxAcceptedInDomainGeneration, int maxAcceptedOuterDomainGeneration)
        {
            urisQueue.EnqueueBaseDomain(baseDomain, maxAcceptedInDomainGeneration, maxAcceptedOuterDomainGeneration);
        }

        private async Task<String> DownloadResourceAsync(Uri uri)
        {
            using (HttpResponseMessage response = await httpClient.GetAsync(uri, HttpCompletionOption.ResponseContentRead))
            {
                if (response.StatusCode != HttpStatusCode.OK) throw new ResourceNotAvailableException();
                using (HttpContent content = response.Content)
                {
                    return await content.ReadAsStringAsync();
                }
            }
        }

        private void RunDownloadingThread()
        {
            while (isRunning)
            {


                Uri uri;
                bool processChildren = urisQueue.Take(out uri);

                Task<String> resultTask = DownloadResourceAsync(uri);

                resultTask.ContinueWith(result => resultsQueue.Enqueue(
                    new CrawlerResult(result.Result, uri, SearchableContentTypes.HTML), 
                    processChildren)
                );
            }
        }
    }
}
