﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NMock2;
using AsyncWebClient;
using PatternMatching;
using Ploter;
using System.Threading;
using System.IO;

namespace Parser
{
    [TestFixture]
    class MainParserTest
    {
        private Crawler _mockCrawler;
        private PatternMatcher _patternMatcher;
        private IReturnResult _ploter;
        private MainParser _parser;
        private Mockery _mockery;
        private List<PatternMatcher> _patternMatchers;

        

        [SetUp]
        public void Setup()
        {
            _mockery = new Mockery();

            // Create a new mock object of the IUserProvider
            _mockCrawler = (Crawler)_mockery.NewMock(typeof(Crawler));
            _patternMatcher = (PatternMatcher)_mockery.NewMock(typeof(PatternMatcher));
            _ploter = (IReturnResult)_mockery.NewMock(typeof(IReturnResult));
            _patternMatchers = new List<PatternMatcher>();
            _patternMatchers.Add(_patternMatcher);

            //Create the concrete object we will be testing
            _parser = new MainParser(_ploter, _patternMatchers);
            
            
        }

        [Test]
        public void TestProperCrawler()
        {

            //First you need to setup the test. These are the Expectations from the test.
            //Crawler crawler = mockCrawler;
            _parser.SetCrawler(_mockCrawler);
            Assert.Pass();    
            //end of test
            // Check that the method returned the correct value
            //Assert.IsFalse(result);
        }

        [Test]
        public void TestProperThreadsCreated()
        {

            List<AbstractParser> parsers = _parser.GetParsers();
            Assert.AreEqual(15,parsers.Count);
        }

        /*[Test]
        public void TestQueueOverloaded()
        {

            Document document = new Document("hej", "adres", true, SearchableContentTypes.HTML);


            Expect.AtLeastOnce.On(_mockCrawler).Method("Take").Will(Return.Value(document));
            _parser.SetCrawler(_mockCrawler);
            Expect.AtLeastOnce.On(_patternMatcher).Method("FindPattern").Will(Return.Value("zdanie "));
            Expect.AtLeastOnce.On(_patternMatcher).Method("GetId").Will(Return.Value(1));
            Thread newParserThread = new Thread(new ThreadStart(_parser.Run));
            newParserThread.Start();
            Thread.Sleep(5000);
            List<AbstractParser> parsers = _parser.GetParsers();
            Assert.AreEqual(15, parsers.Count);
        }*/

        [Test]

        public void TestAddingHtmlToQueue()
        {
            Document document = new Document("adres",true,SearchableContentTypes.HTML,new byte[2]);


            Expect.AtLeastOnce.On(_mockCrawler).Method("Take").Will(Return.Value(document));
            _parser.SetCrawler(_mockCrawler);
            Thread newParserThread = new Thread(new ThreadStart(_parser.Run));
            newParserThread.Start();
            Document parserDocument = _parser.GetDocument(SearchableContentTypes.HTML);
            _parser.EndRun();
            Assert.AreEqual(document,parserDocument);
        }

        [Test]
        public void TestAddingPdfToQueue()
        {
            Document document = new Document("adres", true, SearchableContentTypes.PDF,new byte[2]);


            Expect.AtLeastOnce.On(_mockCrawler).Method("Take").Will(Return.Value(document));
            _parser.SetCrawler(_mockCrawler);
            Thread newParserThread = new Thread(new ThreadStart(_parser.Run));
            newParserThread.Start();
            Document parserDocument = _parser.GetDocument(SearchableContentTypes.PDF);
            _parser.EndRun();
            Assert.AreEqual(document, parserDocument);
        }

        [Test]
        public void TestAddingDocToQueue()
        {
            Document document = new Document( "adres", true, SearchableContentTypes.DOC,new byte [2]);


            Expect.AtLeastOnce.On(_mockCrawler).Method("Take").Will(Return.Value(document));
            _parser.SetCrawler(_mockCrawler);
            Thread newParserThread = new Thread(new ThreadStart(_parser.Run));
            newParserThread.Start();
            Document parserDocument = _parser.GetDocument(SearchableContentTypes.DOC);
            _parser.EndRun();
            Assert.AreEqual(document, parserDocument);
        }

        [Test]
        public void TestRunHTML()
        {
            Document document = new Document( "adres", true, SearchableContentTypes.HTML, File.ReadAllBytes("D:/test.htm"));
            Expect.AtLeastOnce.On(_mockCrawler).Method("Take").Will(Return.Value(document));
            //Expect.AtLeastOnce.On(parser).Method("GetDocument").With(SearchableContentTypes.DOC).Will(Return.Value(document));
            Expect.AtLeastOnce.On(_patternMatcher).Method("FindPattern").Will(Return.Value("zdanie "));
            Expect.AtLeastOnce.On(_patternMatcher).Method("GetId").Will(Return.Value(1));
            Expect.AtLeastOnce.On(_mockCrawler).Method("Enqueue");
            Expect.AtLeastOnce.On(_ploter).Method("putResult");
            _parser.SetCrawler(_mockCrawler);
            Thread newParserThread = new Thread(new ThreadStart(_parser.Run));
            newParserThread.Start();
            //Document parserDocument = parser.GetDocument(SearchableContentTypes.DOC);
            Thread.Sleep(4000);
            _parser.EndRun();
            Assert.Pass();
        }

        [Test]
        public void TestRunDOC()
        {
            Document document = new Document( "adres", true, SearchableContentTypes.DOC, File.ReadAllBytes("D:/test.docx"));
            Expect.Exactly(10).On(_mockCrawler).Method("Take").Will(Return.Value(document));
            //Expect.AtLeastOnce.On(parser).Method("GetDocument").With(SearchableContentTypes.DOC).Will(Return.Value(document));
            Expect.AtLeastOnce.On(_patternMatcher).Method("FindPattern").Will(Return.Value("zdanie "));
            Expect.AtLeastOnce.On(_patternMatcher).Method("GetId").Will(Return.Value(1));
            Expect.AtLeastOnce.On(_mockCrawler).Method("Enqueue");
            Expect.AtLeastOnce.On(_ploter).Method("putResult");
            _parser.SetCrawler(_mockCrawler);
            Thread newParserThread = new Thread(new ThreadStart(_parser.Run));
            newParserThread.Start();
            //Document parserDocument = parser.GetDocument(SearchableContentTypes.DOC);
            Thread.Sleep(4000);
            _parser.EndRun();
            Assert.Pass();
        }

        [Test]
        public void TestRunPDF()
        {
            Document document = new Document( "adres", true, SearchableContentTypes.PDF, File.ReadAllBytes("D:/test.pdf"));
            Expect.AtLeastOnce.On(_mockCrawler).Method("Take").Will(Return.Value(document));
            //Expect.AtLeastOnce.On(parser).Method("GetDocument").With(SearchableContentTypes.DOC).Will(Return.Value(document));
            Expect.AtLeastOnce.On(_patternMatcher).Method("FindPattern").Will(Return.Value("zdanie "));
            Expect.AtLeastOnce.On(_patternMatcher).Method("GetId").Will(Return.Value(1));
            Expect.AtLeastOnce.On(_mockCrawler).Method("Enqueue");
            Expect.AtLeastOnce.On(_ploter).Method("putResult");
            _parser.SetCrawler(_mockCrawler);
            Thread newParserThread = new Thread(new ThreadStart(_parser.Run));
            newParserThread.Start();
            //Document parserDocument = parser.GetDocument(SearchableContentTypes.DOC);
            Thread.Sleep(4000);
            _parser.EndRun();
            Assert.Pass();
        }


        [TearDown]
        public void TearDown()
        {
            // Check that all of the Expectations occured during the test
            _mockery.VerifyAllExpectationsHaveBeenMet();

            //Clean up the objects
            _mockery = null;
            _ploter = null;
            _mockCrawler = null;
            _patternMatchers = null;
            _patternMatcher = null;
            _parser.EndRun();
            _parser = null;
        }
    }
}
