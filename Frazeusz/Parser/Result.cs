﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    public class Result
    {

        public Result(String url, String sentence, int patternMatcherID)
        {
            Url = url;
            Sentence = sentence;
            PatternMatcherID = patternMatcherID;
        }

        public String Url { get; set; }

        public String Sentence { get; set; }

        public int PatternMatcherID { get; set; }
    }
}

