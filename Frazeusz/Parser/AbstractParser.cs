﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternMatching;
using Ploter;
using AsyncWebClient;
using System.Text.RegularExpressions;

namespace Parser
{
    public abstract class AbstractParser
    {
        protected List<PatternMatcher> _patternMatcherList;
        protected IReturnResult _ploter;
        protected Crawler _crawler;
        protected MainParser _mainParser;
        protected List<Uri> _uris;
        protected bool _isRunning;


        protected AbstractParser(List<PatternMatcher> patternMatchers, Crawler crawler, IReturnResult ploter,MainParser mainParser)
        {
            _patternMatcherList = patternMatchers;
            _crawler = crawler;
            _ploter = ploter;
            _mainParser = mainParser;
            _uris = new List<Uri>();
            _isRunning = true;
        }

        

        public void RunByte()
        {
            List<Result> results = new List<Result>();
            List<String> sentences = new List<String>();

            while (_isRunning)
            {
                Document document = GetDocument();
                
                sentences.AddRange(Parse(document.Bytes));
                
                foreach (String sentence in sentences)
                {
                    foreach (PatternMatcher patternMatcher in _patternMatcherList)
                    {
                        String foundedMatch = patternMatcher.FindPattern(sentence);
                        if(foundedMatch!=null)
                            results.Add(new Result(document.URL, foundedMatch, patternMatcher.GetId()));
                    }
                }

                if(document.ReturnURLs)
                    _crawler.Enqueue(new Uri(document.URL,UriKind.RelativeOrAbsolute), GetUrls());
                _uris.Clear();

                foreach (Result result in results)
                    _ploter.putResult(result);

                results.Clear();
                sentences.Clear();

            }
        }

        abstract public Document GetDocument();

        abstract public List<String> Parse(String content);

        abstract public List<String> Parse(byte[] content);

        public List<Uri> GetUrls()
        {
            return _uris;
        }

        protected void FindUrls(string content)
        {
            Regex regEx = new Regex(@"(((http|ftp|https):\/\/)?[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)");
            Uri newUri;
            foreach (Match match in regEx.Matches(content))
                if (Uri.TryCreate(match.Value, UriKind.RelativeOrAbsolute, out newUri)) 
                    _uris.Add(newUri);
        }

        public void EndRun()
        {
            _isRunning = false;
        }


    }
}
