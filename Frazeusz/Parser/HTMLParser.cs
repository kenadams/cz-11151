﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternMatching;
using AsyncWebClient;
using Ploter;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.IO;

namespace Parser
{
    class HTMLParser : AbstractParser
    {
        public HTMLParser(List<PatternMatcher> patternMatchers, Crawler crawler, IReturnResult ploter,MainParser mainParser) : base(patternMatchers,crawler,ploter,mainParser)
        {
        }


        override public List<String> Parse(String content)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(content);
            StringBuilder sb = new StringBuilder();
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//p"))
            {
                if (node.Attributes.Count == 0)
                {
                    foreach (HtmlNode link in node.Descendants("a"))
                        _uris.Add(new Uri(link.Attributes["href"].Value));
                }
                sb.AppendLine(node.InnerText);
            }

            string text = sb.ToString();
            FindUrls(text);

            List<String> sentences = new List<String>();

            Regex rx = new Regex(@"(\S.+?[.!?])(?=\s+|$)");
            foreach (Match match in rx.Matches(text))
                sentences.Add(match.Value);

            return sentences;
        }

        override public List<String> Parse(byte[] content)
        {

            HtmlDocument doc = new HtmlDocument();
            doc.Load(new MemoryStream(content));
            StringBuilder sb = new StringBuilder();
            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//p"))
            {
                if (node.Attributes.Count == 0)
                {
                    foreach (HtmlNode link in node.Descendants("a"))
                        _uris.Add(new Uri(link.Attributes["href"].Value));
                }
                sb.AppendLine(node.InnerText);
            }
            string text = polishChars(sb.ToString());
            FindUrls(text);

            List<String> sentences = new List<String>();

            Regex rx = new Regex(@"(\S.+?[.!?])(?=\s+|$)");
            foreach (Match match in rx.Matches(text))
                sentences.Add(match.Value);

            return sentences;
        }


        override public Document GetDocument()
        {
            Document document = null;
            while ((document = _mainParser.GetDocument(SearchableContentTypes.HTML)) == null) ;
            return document;
        }

        private string polishChars(String final)
        {
            Char[] arrayChar = final.ToCharArray();


            StringBuilder sb = new StringBuilder();
            int size = arrayChar.Length;
            int[] array = new int[size];
            for (int i = 0; i < size; i++)
                array[i] = Convert.ToInt32(arrayChar[i]);
             
            for (int i = 0; i < size; i++)
            {
                if (array[i] < 128)
                    sb.Append(((char)array[i]).ToString());
                else
                {
                    sb.Append(getCharFromCode(array[i] + array[i + 1]));
                    i++;
                }
            }

            return sb.ToString();
        }

        private String getCharFromCode(int code){
            switch (code)
            {
                case 8535:
                    return "ń";
                    
                case 8421:
                    return "ć";
                    
                case 8678:
                    return "ę";
                    
                case 8426:
                    return "ą";
                    
                case 664:
                    return "ź";
                    
                case 630:
                     return "ż";
                    
                case 8563:
                     return "ś";
                    
                case 580:
                     return "ó";
                    
                case 8531:
                     return "ł";
                    
                case 444:
                     return "Ń";
                    
                case 574:
                     return "Ź";
                    
                case 500:
                     return "Ż";
                     
                case 442:
                     return "Ł";
                    
                case 8478:
                     return "Ó";
                    
                case 348:
                     return "Ę";
                    
                case 666:
                     return "Ś";
                     
                case 8420:
                     return "Ć";
                     
                case 8418:
                     return "Ą";
                     
                default:
                    return null;

            }
        }


         
    }
}
