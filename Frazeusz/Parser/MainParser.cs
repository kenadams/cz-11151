﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncWebClient;
using Ploter;
using PatternMatching;
using System.Threading;

namespace Parser
{
    public class MainParser : ISetCrawler
    {
            private Crawler _crawler;
            private IReturnResult _ploter;
            private List<PatternMatcher> _patternMatcherList;
            private ConcurrentQueue<Document> _htmlDocumentQueue;
            private ConcurrentQueue<Document> _docDocumentQueue;
            private ConcurrentQueue<Document> _pdfDocumentQueue;
            private const int _maxQueueSize = 5;
            private bool _isRunning;
            private List<AbstractParser> _parsers;


            public void SetCrawler(Crawler crawler)
            {
                _crawler = crawler;
            }

            public MainParser(IReturnResult ploter, List<PatternMatcher> patternMatcherList) {
                _ploter = ploter;
                _patternMatcherList = patternMatcherList;
                SetCrawler(null);
                _isRunning = true;
                _htmlDocumentQueue = new ConcurrentQueue<Document>();
                _docDocumentQueue = new ConcurrentQueue<Document>();
                _pdfDocumentQueue = new ConcurrentQueue<Document>();
                _parsers = new List<AbstractParser>();
                _parsers.AddRange(CreateParsers(SearchableContentTypes.PDF));
                _parsers.AddRange(CreateParsers(SearchableContentTypes.DOC));
                _parsers.AddRange(CreateParsers(SearchableContentTypes.HTML));
           
            }

            public MainParser()
            {

            }

            public void StartParsers(){
                foreach (AbstractParser parser in _parsers)
                {
                    Thread newParserThread = new Thread(new ThreadStart(parser.RunByte));
                    newParserThread.Start();
                }
            }

            public void Run(){
                while (_isRunning)
                {
                    Document document = _crawler.Take();

                    if (document.ContentType == SearchableContentTypes.PDF)
                    {
                        if (_pdfDocumentQueue.Count == _maxQueueSize)
                            _parsers.AddRange(CreateParsers(SearchableContentTypes.PDF));
                    }
                    else if (document.ContentType == SearchableContentTypes.DOC)
                    {
                        if (_docDocumentQueue.Count == _maxQueueSize)
                            _parsers.AddRange(CreateParsers(SearchableContentTypes.DOC));
                    }
                    else if (document.ContentType == SearchableContentTypes.HTML)
                    {
                        if (_htmlDocumentQueue.Count == _maxQueueSize)
                            _parsers.AddRange(CreateParsers(SearchableContentTypes.HTML));
                    }

                    PutDocument(document);
                }
            }

            private List<AbstractParser> CreateParsers(SearchableContentTypes type){
                AbstractParserFactory factory = null;

                if(type==SearchableContentTypes.PDF)
                    factory = new PdfParserFactory();
                
                else if(type==SearchableContentTypes.DOC)
                    factory = new DocParserFactory();
                
                else if (type == SearchableContentTypes.HTML)
                    factory = new HTMLParserFactory();
                      
                AbstractParser [] parsers = factory.CreateParsers(_patternMatcherList,_crawler,_ploter,this);

                foreach(AbstractParser parser in parsers){
                    Thread newParserThread = new Thread(new ThreadStart (parser.RunByte));
                    newParserThread.Start();
                }

                return new List<AbstractParser>(parsers);
            
            }

            private void PutDocument(Document document){
                if (document.ContentType == SearchableContentTypes.PDF)
                    _pdfDocumentQueue.Enqueue(document);
                else if (document.ContentType == SearchableContentTypes.DOC)
                    _docDocumentQueue.Enqueue(document);
                else if (document.ContentType == SearchableContentTypes.HTML)
                    _htmlDocumentQueue.Enqueue(document);      
            }

            public virtual Document GetDocument(SearchableContentTypes type)
            {
                Document document = null;

                if (type == SearchableContentTypes.PDF)
                    _pdfDocumentQueue.TryDequeue(out document);
                else if (type == SearchableContentTypes.DOC)
                    _docDocumentQueue.TryDequeue(out document);
                else if (type == SearchableContentTypes.HTML)
                    _htmlDocumentQueue.TryDequeue(out document);

                return document;
            }

            public void EndRun()
            {
                _isRunning = false;
                foreach (AbstractParser parser in _parsers)
                {
                    parser.EndRun();
                }
            }

            public List<AbstractParser> GetParsers()
            {
                return _parsers;
            }
            
    }


}
