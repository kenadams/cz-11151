﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternMatching;
using AsyncWebClient;
using Ploter;

namespace Parser
{
    class PdfParserFactory : AbstractParserFactory
    {
        override public AbstractParser[] CreateParsers(List<PatternMatcher> patternMatchers, Crawler crawler, IReturnResult ploter,MainParser mainParser)
        {
            PDFParser[] PDFParsers = new PDFParser[_threadsToCreate];

            for (int i = 0; i < _threadsToCreate; i++)
            {
                PDFParsers[i] = new PDFParser(patternMatchers, crawler, ploter,mainParser);
            }

            return PDFParsers;
        }
    }
}
