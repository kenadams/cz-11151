﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternMatching;
using AsyncWebClient;
using Ploter;

namespace Parser
{
    abstract class AbstractParserFactory
    {
        protected const int _threadsToCreate = 5;

        abstract public AbstractParser[] CreateParsers(List<PatternMatcher> patternMatchers, Crawler crawler, IReturnResult ploter, MainParser mainParser);
    }
}
