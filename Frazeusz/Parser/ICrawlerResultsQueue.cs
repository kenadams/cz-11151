﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;

namespace AsyncWebClient
{
    interface ICrawlerResultsQueue
    {
        Document Take();
    }
}
