﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    public class Result
    {

        public Result(String url, String sentence, int patternMatcherID)
        {
            this.url = url;
            this.sentence = sentence;
            this.patternMatcherID = patternMatcherID;
        }

        public String url { get; set; }

        public String sentence { get; set; }

        public int patternMatcherID { get; set; }
    }
}

