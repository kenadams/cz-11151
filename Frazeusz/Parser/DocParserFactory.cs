﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternMatching;
using AsyncWebClient;
using Ploter;

namespace Parser
{
    class DocParserFactory : AbstractParserFactory
    {
        override public AbstractParser[] CreateParsers(List<PatternMatcher> patternMatchers, Crawler crawler, IReturnResult ploter , MainParser mainParser)
        {
            DocParser [] DocParsers = new DocParser[_threadsToCreate];

            for (int i = 0; i < _threadsToCreate; i++)
            {
                DocParsers[i] = new DocParser(patternMatchers,crawler,ploter,mainParser);
            }

            return DocParsers;
        }
    }
}
