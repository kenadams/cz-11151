﻿using System;
using System.Text;

namespace Parser
{
    public class Document
    {
        
        public SearchableContentTypes ContentType{get; set;}
        public String URL{get; set;}
        public bool ReturnURLs{get; set;}

        public byte[] Bytes { get; set; }

        public Document(String URL, bool returnURLs, SearchableContentTypes type, byte[] bytes)
        {
            this.URL = URL;
            ReturnURLs = returnURLs;
            ContentType = type;
            Bytes = bytes;

        }

    }
}
