﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
using System.IO;

//klasa tymczasowa

namespace AsyncWebClient
{
    public class Crawler : ICrawlerResultsQueue
    {
            Document _doc;
            Document _pdf;
            Document _html;
            Random _random;

            public Crawler()
            {
                _doc = new Document("www.google.com", true, SearchableContentTypes.DOC, File.ReadAllBytes("../../testfiles/test.docx"));
                _pdf = new Document("www.google.com", true, SearchableContentTypes.PDF, File.ReadAllBytes("../../testfiles/test.pdf"));
                _html = new Document("www.google.com", true, SearchableContentTypes.HTML, File.ReadAllBytes("../../testfiles/test.htm"));
                _random = new Random();
            }

            public virtual Document Take()
            {
                int nr = _random.Next(1, 4);
                if (nr == 1)
                    return _doc;
                if (nr == 2)
                    return _pdf;
                else
                    return _html;
            }

            virtual public void Enqueue(Uri uri, List<Uri> uris)
            {

            }
    }
}
