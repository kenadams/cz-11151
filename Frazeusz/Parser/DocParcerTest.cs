﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NMock2;
using AsyncWebClient;
using PatternMatching;
using Ploter;
using System.Threading;
using System.IO;
using Parser;

namespace ParserTest
{
    [TestFixture]
    class DocParserTest
    {
        private Crawler _mockCrawler;
        private PatternMatcher _patternMatcher;
        private IReturnResult _ploter;
        private MainParser _parser;
        private Mockery _mockery;
        private List<PatternMatcher> _patternMatchers;
        private DocParser _docParser;

        [SetUp]
        public void Setup()
        {
            _mockery = new Mockery();

            _mockCrawler = (Crawler)_mockery.NewMock(typeof(Crawler));
            _patternMatcher = (PatternMatcher)_mockery.NewMock(typeof(PatternMatcher));
            _ploter = (IReturnResult)_mockery.NewMock(typeof(IReturnResult));
            _patternMatchers = new List<PatternMatcher>();
            _patternMatchers.Add(_patternMatcher);
            _parser = _mockery.NewMock<MainParser>();

            _docParser = new DocParser(_patternMatchers, _mockCrawler, _ploter, _parser);


        }

        [Test]
        public void TestGetDocument()
        {
            Document document = new Document( "adres", true, SearchableContentTypes.DOC,new byte [2]);
            Expect.AtLeastOnce.On(_parser).Method("GetDocument").With(SearchableContentTypes.DOC).Will(Return.Value(document));
            Document parserDocument = _docParser.GetDocument();

            Assert.AreEqual(document, parserDocument);
        }

        [Test]
        public void TestGetDocumentWait()
        {
            Document document = new Document("adres", true, SearchableContentTypes.DOC, new byte[2]);
            Expect.AtLeastOnce.On(_parser).Method("GetDocument").With(SearchableContentTypes.DOC).Will(Return.Value(null));
            Thread newParserThread = new Thread(new ThreadStart(_docParser.RunByte));
            newParserThread.Start();
            Expect.Never.On(_mockCrawler).Method("Enqueue");
            Expect.Never.On(_ploter).Method("putResult");
            Expect.Never.On(_patternMatcher).Method("FindPattern");

            Thread.Sleep(2000);
            Assert.Pass();
        }

        [Test]
        public void TestParse()
        {
            List<String> sentences = new List<string>() { "I have an idea.", "What about killing someone.", "That would be fun." };
            List<String> sentencesFromParser = _docParser.Parse(File.ReadAllBytes("../../testfiles/test.docx"));

            Assert.AreEqual(sentences.Count, sentencesFromParser.Count);
        }

        
        [Test]
        public void TestFindUrls()
        {
            List<Uri> uris = new List<Uri>() { new Uri("http\\:www.google.com", UriKind.RelativeOrAbsolute) };
            List<String> sentences = new List<string>() { "I have an idea.", "What about killing someone.", "That would be fun." };
            List<String> sentencesFromParser = _docParser.Parse(File.ReadAllBytes("../../testfiles/test.docx"));
            List<Uri> parserUris = _docParser.GetUrls();
            Assert.AreEqual(uris.Count, parserUris.Count);
        }

        [Test]
        public void TestRun()
        {

            Document document = new Document( "www.google.com", true, SearchableContentTypes.DOC, File.ReadAllBytes("../../testfiles/test.docx"));
            Expect.AtLeastOnce.On(_parser).Method("GetDocument").With(SearchableContentTypes.DOC).Will(Return.Value(document));
            Expect.AtLeastOnce.On(_patternMatcher).Method("FindPattern").Will(Return.Value("zdanie "));
            Expect.AtLeastOnce.On(_patternMatcher).Method("GetId").Will(Return.Value(1));
            Expect.AtLeastOnce.On(_mockCrawler).Method("Enqueue");
            Expect.AtLeastOnce.On(_ploter).Method("putResult");

            Thread newParserThread = new Thread(new ThreadStart(_docParser.RunByte));
            newParserThread.Start();

            Thread.Sleep(2000);
            Assert.Pass();

        }

        [Test]
        public void TestBadParse()
        {

            List<String> sentences = new List<string>() { "I have an idea.", "What about killing someone.", "That would be fun." };
            Assert.Throws<ICSharpCode.SharpZipLib.Zip.ZipException>(() => { _docParser.Parse(File.ReadAllBytes("../../testfiles/nowy.txt")); });
        }

        [TearDown]
        public void TearDown()
        {
            _mockery.VerifyAllExpectationsHaveBeenMet();

            _mockery = null;
            _ploter = null;
            _mockCrawler = null;
            _patternMatchers = null;
            _patternMatcher = null;
            _parser = null;
            _docParser.EndRun();
            _docParser = null;
        }
    }
}
