﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NMock2;
using AsyncWebClient;
using PatternMatching;
using Ploter;
using System.Threading;
using System.IO;
using Parser;

namespace ParserTest
{
    [TestFixture]
    class GeneralTest
    {
  
        class MyPloter : IReturnResult
        {
            public Result Result {get; set;}

            public MyPloter()
            {
                Result = null;

            }

            public void putResult(Result result)
            {
                Result = result;
            }
        }


        private Crawler _mockCrawler;
        private PatternMatcher _patternMatcher;
        private MainParser _parser;
        private Mockery _mockery;
        private List<PatternMatcher> _patternMatchers;
        private MyPloter _ploter;

        [SetUp]
        public void Setup()
        {
            _mockery = new Mockery();

            _mockCrawler = new Crawler();
            _patternMatcher = (PatternMatcher)_mockery.NewMock(typeof(PatternMatcher));
            _ploter = new MyPloter();
            _patternMatchers = new List<PatternMatcher>();
            _patternMatchers.Add(_patternMatcher);
            _parser = new MainParser(_ploter,_patternMatchers);
            _parser.SetCrawler(_mockCrawler);

        }

       
        [Test]
        public void TestRun()
        {
            Expect.AtLeastOnce.On(_patternMatcher).Method("FindPattern").Will(Return.Value("zdanie "));
            Expect.AtLeastOnce.On(_patternMatcher).Method("GetId").Will(Return.Value(1));

            Thread newParserThread = new Thread(new ThreadStart(_parser.Run));
            newParserThread.Start();

            Thread.Sleep(4000);
            Assert.AreEqual(1, _ploter.Result.PatternMatcherID);
            Assert.AreEqual("www.google.com", _ploter.Result.Url);
            Assert.AreEqual("zdanie ", _ploter.Result.Sentence);

        }








        [TearDown]
        public void TearDown()
        {
            // Check that all of the Expectations occured during the test
            _mockery.VerifyAllExpectationsHaveBeenMet();

            //Clean up the objects
            _mockery = null;
            _ploter = null;
            _mockCrawler = null;
            _patternMatchers = null;
            _patternMatcher = null;
            _parser.EndRun();
            _parser = null;
            //docParser = null;
        }
    }
}
