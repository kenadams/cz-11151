﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crawler;
using Frazeusz.PatternMatcher;
using NMock2;
using NUnit.Framework;
using Parser;
using PatternMatcher;

namespace Ploter
{
    [TestFixture]
    class PloterTest
    {
        private Mockery _mockery;
        private MainPloter _ploter;
        private Crawler.Crawler _mockCrawler;
        private Monitor.Monitor _monitor;
        private MainParser _mockParser;

        [SetUp]
        public void SetUp()
        {
            _mockery = new Mockery();
            _mockCrawler = (Crawler.Crawler)_mockery.NewMock(typeof(Crawler.Crawler));
            _monitor = (Monitor.Monitor) _mockery.NewMock(typeof (Monitor.Monitor));
            _mockParser = (MainParser) _mockery.NewMock(typeof (MainParser));
            _ploter = new MainPloter(new Collection<ExtendedResult>());

        }

        [Test]
        public void TestStartSearch()
        {
            var uris = new List<string> {"google.pl", "onet.pl", "interia.pl" };
            Expect.Once.On(_mockParser).Method("Run");
            Expect.Once.On(_mockCrawler).Method("Start");
                
            bool res = _ploter.StartSearch();
            Assert.True(res);

            Assert.Pass();
        }
        [Test]
        public void TestStopSearch()
        {
            Assert.Equals(true, _ploter.StopSearch());
            Expect.AtLeastOnce.On(_mockCrawler).Method("IsRunning").Will(Return.Value(false));
            Assert.Pass();
        }
        [Test]
        public void TestIsSearchInProgress()
        {
            Assert.True(_ploter.IsSearchInProgress());
            _mockCrawler = null;
            Assert.False(_ploter.IsSearchInProgress());
        }

        [TearDown]
        public void TearDown()
        {
            _mockery.VerifyAllExpectationsHaveBeenMet();
            _mockery = null;
            _mockCrawler = null;
            _monitor = null;
            _mockParser = null;
            _ploter = null;

        }
    }
}
