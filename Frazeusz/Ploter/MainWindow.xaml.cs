﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Ploter;


namespace Ploter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<Domain> DomainList { get; set; }
        private ObservableCollection<Pattern> PatternList { get; set; }
        private ObservableCollection<ExtendedResult> Results { get; set; }
        private ObservableCollection<KeyValuePair<string, int>> graphData = new ObservableCollection<KeyValuePair<string, int>>();
    
        public MainWindow()
        {
            InitializeComponent();

            //This is for testing purposes
            DomainList = new ObservableCollection<Domain>();
            PatternList = new ObservableCollection<Pattern>();
            Results = new ObservableCollection<ExtendedResult>();
            DomainList.Add(new Domain { DomainName = "www.onet.pl" });
            DomainList.Add(new Domain { DomainName = "www.allegro.pl" });
            DomainList.Add(new Domain { DomainName = "www.wykop.pl" });
            PatternList.Add(new Pattern { PatternName = "Ala ma kota" });
            PatternList.Add(new Pattern { PatternName = "Kot ma Ale" });
            PatternList.Add(new Pattern { PatternName = "Jarek ma psa" });

            Strony.ItemsSource = DomainList;
            Strony.DataContext = DomainList;
            Wzorce.ItemsSource = PatternList;
            Wzorce.DataContext = PatternList;
            columnChart.DataContext = graphData;
            
            
            ResultsListView.ItemsSource = Results;
            ResultsListView.DataContext = Results;
            ExtendedResult result = new ExtendedResult("www.onet.pl", "hello geeki", 5);
            result.Pattern = "hello";
            Results.Add(result);

        }


        //only for testing
        public class Domain
        {
            public string DomainName { get; set; }

        }

        //only for testing
        public class Pattern
        {
            public string PatternName { get; set; }
        }

        public void CreateCheckBoxList()
        {
            
        }

        private void Strona_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void showColumnChart()
        {
           
        }

        private void onDrawButtonClick(object sender, RoutedEventArgs e)
        {
            graphData.Add(new KeyValuePair<string, int>("www.onet.pl", 5));
            graphData.Add(new KeyValuePair<string, int>("www.allegro.pl", 7));
            graphData.Add(new KeyValuePair<string, int>("www.wykop.pl", 11));
            columnChart.DataContext = graphData;
        }

        
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            monitor.Visibility = Visibility.Visible;

        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            monitor.Visibility = Visibility.Hidden;
        }

    }
}
